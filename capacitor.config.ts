import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'com.app.ionic',
  appName: 'app-ionic',
  webDir: 'www',
  bundledWebRuntime: false
};

export default config;
