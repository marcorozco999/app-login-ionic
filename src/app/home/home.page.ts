import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  constructor(private router: Router) {}

  goRegister(){
    try {
      this.router.navigate(['/register']);
    } catch (error) {
      console.log(error);
    }
  }

  goLogin(){
    try {
      this.router.navigate(['/login']);
    } catch (error) {
      console.log(error);
    }
  }

}
