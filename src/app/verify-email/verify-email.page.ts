import { Router } from '@angular/router';
import { AuthService } from './../services/auth.service';
import { Observable } from 'rxjs';
import { Component, OnInit } from '@angular/core';
import { User } from '../shared/user.interface';
import { promise } from 'protractor';

@Component({
  selector: 'app-verify-email',
  templateUrl: './verify-email.page.html',
  styleUrls: ['./verify-email.page.scss'],
})
export class VerifyEmailPage implements OnInit {
 user$: Observable<User> = this.authSvc.afAuth.user;
  constructor(private authSvc: AuthService, private router: Router) { }

  ngOnInit() {
  }

  async onSendEmail(): Promise<void>{
    try {
      await this.authSvc.sendVerificationEmail();
      this.router.navigate(['/login']);
    } catch (error) {
      console.log('Error -> ', error);
    }
  }

}
