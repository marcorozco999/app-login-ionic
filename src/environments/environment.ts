// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyDES8mL2pE5l1InKFmh2OKBVebTrm-V23I",
    authDomain: "ionic-login-d5dc6.firebaseapp.com",
    databaseURL: "https://ionic-login-d5dc6-default-rtdb.firebaseio.com",
    projectId: "ionic-login-d5dc6",
    storageBucket: "ionic-login-d5dc6.appspot.com",
    messagingSenderId: "145807832382",
    appId: "1:145807832382:web:2a305a982558e90aa6a86f",
    measurementId: "G-RV3RK3Y2ZV"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
